import pygame


class Surface(pygame.Surface):

    def __init__(self, x, y, width, height, color):
        super(Surface, self).__init__((width, height))
        self.x = x
        self.width = width
        self.height = height
        self.y = y
        self.offset_x = 0
        self.offset_y = 0

        self.final_x = self.x
        self.final_y = self.y

        self.fill(color)
        self.set_alpha(255)


    def animate(self, speed=1):

        if self.offset_x > 0:
            self.offset_x = round(self.offset_x + -speed, 1)
            self.x += speed
        elif self.offset_x < 0:
            self.offset_x += round(self.offset_x + speed, 1)
            self.x -= speed
        else:
            self.x = self.final_x

        if self.offset_y > 0:
            self.offset_y += round(self.offset_x + -speed, 1)
            self.y += speed
        elif self.offset_y < 0:
            self.offset_y += round(self.offset_x + speed, 1)
            self.y -= speed
        else:
            self.y = self.final_y

        return False if (self.offset_x == 0 and self.offset_y == 0) else True

    def move(self, ox, oy):
        self.offset_x, self.offset_y = ox, oy
        self.final_x = self.x + ox
        self.final_y = self.y + oy

    def render(self, screen):
        render_x = round(self.x - self.width/2)
        render_y = round(self.y - self.height/2)
        screen.blit(self, (render_x, render_y))



