from buildings.building import Building
from game import colors, settings
from entity.units.unit_factory import UnitFactory
import random
from grid.grid import Grid
from helpers.helper import Helper
from entity.surface.surface import Surface


class TownHall(Building):

    SPAWN_TIME = 2
    AREA = 16

    def __init__(self, x, y, team):
        super(TownHall, self).__init__(team)
        self.cell = Grid().register(x, y, self)
        self.surface = Surface(self.cell.x, self.cell.y, settings.CELL_WIDTH * 5, settings.CELL_HEIGHT * 5, colors.YELLOW)

        self.spawn_cells = Helper.buildingHelper.spawn_cells(self.cell.x, self.cell.y, self.AREA)

        self.timer = Helper.timerHelper.now()
        self.walk_timer = Helper.timerHelper.now()
        self.state = 0


    def update(self):
        self.spawn_unit()

        if Helper.timerHelper.elapsed(self.walk_timer, 0.2) and self.state == 0:

            Grid().move(16, 0, self)
            self.surface.move(8, 0)
            self.state = 1
            self.walk_timer = Helper.timerHelper.now()

        if self.surface.animate(speed=0.1):
            # animation done
            self.state = 0

        Helper.animationHelper.blink(self.surface, speed=0.6, min=75, max=150)


    def render(self, screen):
        self.surface.render(screen)

    def collide(self, other):
        pass

    def spawn_unit(self):
        if Helper.timerHelper.elapsed(self.timer, self.SPAWN_TIME):

            wx, wy = random.choice(self.spawn_cells)
            UnitFactory.worker(wx, wy, self.team)

            self.timer = Helper.timerHelper.now()


    # def spawn_unit(self, unit_class):
    #     diff = (datetime.now() - self.timer).total_seconds() > self.SPAWN
    #     if diff:
    #         for field in self.fields:
    #             x, y = field
    #             if not any(unit.rect.collidepoint(x, y) for unit in settings.UNITS):
    #                 unit_class(x, y, self.team)
    #                 break
    #         self.timer = datetime.now()