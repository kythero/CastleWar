from collections import OrderedDict


class Resources(object):

    USER = OrderedDict(
        gold=120,
        woods=0,
        boards=0,
    )
    ENEMY = OrderedDict(
        gold=120,
        woods=0,
        boards=0,
    )

    @classmethod
    def player(cls, team):
        return {
            1: cls.USER,
            2: cls.ENEMY
        }[team]

    @classmethod
    def add_boards(cls, team, boards):
        cls.player(team)['boards'] += boards

    @classmethod
    def add_gold(cls, team, gold):
        cls.player(team)['gold'] += gold

    @classmethod
    def add_woods(cls, team, woods):
        cls.player(team)['woods'] += woods

    @classmethod
    def remove_boards(cls, team, boards):
        cls.player(team)['boards'] -= boards

    @classmethod
    def remove_gold(cls, team, gold):
        cls.player(team)['gold'] -= gold

    @classmethod
    def remove_woods(cls, team, woods):
        cls.player(team)['woods'] -= woods
