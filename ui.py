import pygame
from game import colors
from datetime import datetime
from game import settings
from sprites.ui.sprite import Sprite
from itertools import ifilter
from buildings.lumberman_cottage import LumbermanCottage
from bank import Resources

class UserInterface(object):

    INCOME_DELAY = 3

    def __init__(self, main):
        self.main = main
        self.pencil = pygame.font.SysFont("monospace", 14, bold=True)

        self.income = 1
        self.timer = datetime.now()
        self.sprites = []
        self.init_sprites()
        self.team = 1

        self.select = None
        self.select_rect = pygame.Rect(0, 0, 0, 0)

    def init_sprites(self):
        self.sprites.append(
            Sprite(settings.WIDTH-24-8, 0+8, 24, 24, colors.RED)
        )

    def events(self):
        if self.select:
            x, y = pygame.mouse.get_pos()
            self.select_rect.x = x - x%8 - 16
            self.select_rect.y = y - y%8 - 16
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.main.RUNNING = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if self.select:

                    self.select(self.select_rect.x, self.select_rect.y, self.team)
                    self.select = None
                else:
                    for sprite in self.sprites:
                        if sprite.rect.collidepoint(event.pos):
                            self.select_rect = sprite.rect.move(0,0)
                            self.select = sprite.building


    def increase_income(self, amount):
        self.income += round(amount * 0.2)

    def update(self):
        self.events()

        diff = (datetime.now() - self.timer).total_seconds() > self.INCOME_DELAY
        if diff:
            Resources.add_gold(self.team, self.income)
            self.timer = datetime.now()

    def render(self, screen):
        gold = self.pencil.render('Income: ' + str(Resources.player(self.team)['gold']), 1, colors.YELLOW)
        screen.blit(gold, (10, 10))

        lumber = self.pencil.render('woods: ' + str(Resources.player(self.team)['woods']), 1, colors.YELLOW)
        screen.blit(lumber, (120, 10))

        boards = self.pencil.render('Boards: ' + str(Resources.player(self.team)['boards']), 1, colors.YELLOW)
        screen.blit(boards, (230, 10))

        for sprite in self.sprites:
            sprite.render(screen)

        if self.select:
            pygame.draw.rect(screen, colors.GREEN, self.select_rect, 1)

