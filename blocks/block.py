from game import settings
from game import colors
import pygame


class Block(object):

    def __init__(self, x, y):
        self.destroyable = False
        self.color = colors.GREEN
        self.name = "Wall"
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH, settings.CELL_HEIGHT)
        settings.OBSTACLES.append(self)

    def __str__(self):
        return self.name

    def update(self):
        pass

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)
