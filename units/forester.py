from units.unit import Unit
import pygame
from game import settings, colors
from game.settings import States
from datetime import datetime
from resources.tree import Tree
import logic
import random
from itertools import ifilter


class Forester(Unit):

    DAMAGE = 1
    DELAY = 0.1  # seconds
    HP = 40
    ATTACK_RANGE = 12
    RANGE = 20
    VISION = 32
    SPAWN_TREE = 2
    TIME_FOR_TREE = 3
    SPEED = 0.002

    def __init__(self, x, y, team, house):
        super(Forester, self).__init__(team)
        self.house = house
        self.name = "Forester"
        self.hp = self.HP
        self.color = colors.RED
        self.rect = pygame.Rect(x, y, settings.UNIT_WIDTH, settings.UNIT_HEIGHT)
        self.state = States.idle
        self.target = None
        self.hold_tree = False
        self.seed_time = None
        self.walk_timer = datetime.now()

    def __str__(self):
        return self.name

    def get_tree_location(self):
        all_fields = set(self.house.places_for_tree)
        fields = set([
            o.rect.topleft for o in ifilter(
                lambda o: o.rect.topleft in all_fields, settings.UNITS + settings.RESOURCES
            )
        ])
        try:
            location = random.choice(list(all_fields - fields))
        except IndexError:
            location = None
        return location

    def update(self):
        if self.state == States.idle:
            diff = (datetime.now() - self.timer).total_seconds() > self.SPAWN_TREE
            if diff:
                for field in self.house.fields:
                    if not any(unit.rect.collidepoint(field) for unit in settings.UNITS):
                        self.target = self.get_tree_location()
                        if self.target:
                            self.rect.topleft = field
                            self.state = States.move
                            self.hold_tree = True
                        break

        elif self.state == States.move:
            if logic.distance2(self.target, self.rect.topleft) < self.RANGE:
                if self.hold_tree:
                    x, y = self.target
                    Tree(x, y)
                    self.hold_tree = False
                    self.target = self.house.rect.topleft
                else:
                    self.state = States.idle
                    self.rect.x = -8
                    self.rect.y = -8
                    self.timer = datetime.now()
            else:
                diff = (datetime.now() - self.walk_timer).total_seconds() > self.SPEED
                if diff:
                    _, direction = logic.shortest_path(self, self.target)
                    self.move2(direction)
                    self.walk_timer = datetime.now()

    def render(self, screen):
        pygame.draw.ellipse(screen, self.color, self.rect.move(-4, -4))