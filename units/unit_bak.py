import pygame
from game import settings
from game import colors
from datetime import datetime
from random import shuffle
import math

import logging

LOGGER = logging.getLogger(__name__)

WALK_SCENARIOS = {
    'left': ['top', 'bottom', 'top_left', 'bottom_left'],
    'top': ['left', 'right', 'top_left', 'top_right'],
    'right': ['top', 'bottom', 'top_right', 'bottom_right'],
    'bottom': ['left', 'right', 'bottom_left', 'bottom_right'],
    'top_right': ['top', 'right'],
    'top_left': ['top', 'left'],
    'bottom_right': ['bottom', 'right'],
    'bottom_left': ['bottom', 'left']
}

STATES = {
    'attack': 2,
    'move': 1,
    'idle': 0,
}


class Unit(object):

    DELAY = 0.1 # seconds
    DEATH = 2
    HP = 60

    def __init__(self, x, y, team):
        self.hp = self.HP
        self.damage = 0
        self.name = "Jednostka"
        self.vision = 32
        self.attack_range = 9
        self.team = team
        self.dead = False
        self.target = (0, 0)
        if self.team == 1:
            self.main_target = (384, 384)
        else:
            self.main_target = (32, 32)

        self.state = STATES['idle']
        self.color = colors.WHITE
        self.rect = pygame.Rect(x, y, settings.UNIT_WIDTH, settings.UNIT_HEIGHT)

        self.timer = datetime.now()
        self.time_death = None
        self.scan_target()
        settings.UNITS.append(self)

    def __str__(self):
        return "({0}):{1}".format(self.team, self.name)

    def distance(self, unit):
        tx, ty = unit.rect.center
        cx, cy = self.rect.center
        return math.sqrt((tx - cx)**2 + (ty-cy)**2)

    def scan_target(self):
        available_units = []
        for unit in settings.UNITS:
            if unit == self:
                continue

            distance = self.distance(unit)
            if distance <= self.vision:
                available_units.append((unit, distance))

        available_units.sort(key=lambda t: t[1])
        if available_units:
            selected_unit, _ = available_units[0]
            self.target_unit = selected_unit
            self.target = (selected_unit.rect.x, selected_unit.rect.y)
        else:
            self.target_unit = None
            self.target = self.main_target

        self.state = STATES['move']

    def get_direction(self):
        cx, cy = self.rect.x, self.rect.y
        tx, ty = self.target

        diff_x = tx - cx
        diff_y = ty - cy
        try:
            dir_x = (diff_x / abs(diff_x)) * settings.CELL_WIDTH
        except ZeroDivisionError:
            dir_x = 0
        try:
            dir_y = (diff_y / abs(diff_y)) * settings.CELL_HEIGHT
        except ZeroDivisionError:
            dir_y = 0

        direction = (0, 0)
        for k, v in settings.DIRECTIONS.iteritems():
            if v == (dir_x, dir_y):
                direction = k
                break

        return direction

    def get_walk_scenario(self, dir):
        walk_dirs = WALK_SCENARIOS[dir]
        shuffle(walk_dirs)
        walk_dirs.insert(0, dir)
        return walk_dirs

    def death(self):
        if not self.dead and self.hp <= 0:
            self.dead = True
            self.time_death = datetime.now()

        if self.time_death and (datetime.now() - self.time_death).total_seconds() > self.DEATH:
            settings.UNITS.remove(self)

    def resurrection(self):
        self.hp = self.HP
        self.dead = False
        self.time_death = None

    def update(self):
        self.death()

        if not self.dead:
            self.scan_target()
            diff = (datetime.now() - self.timer).total_seconds() > self.DELAY
            if diff:
                if self.target_unit and self.distance(self.target_unit) <= self.attack_range:
                    self.state = STATES['attack']
                    self.target_unit.hp = self.target_unit.hp - self.damage

                elif self.state == STATES['move']:
                    direction = self.get_direction()
                    dirs = self.get_walk_scenario(direction)
                    self.move(dirs)
                self.timer = datetime.now()


    def move(self, possible_directions):
        """
        :param possible_directions: List of possible directions, if one of them is blocked.
        dirs should be defined like keys in settings.DIRECTIONS. For example ['left', 'top', 'bottom'].
        """
        for dir in possible_directions:
            x, y = settings.DIRECTIONS[dir]
            rect = self.rect.move(x, y)

            blocked_by_block = False
            if any(block.rect.colliderect(rect) for block in settings.OBSTACLES):
                blocked_by_block = True
                LOGGER.debug("[%s] collides in dir:[%s] with block", self, dir)

            blocked_by_unit = False
            if any(unit.rect.colliderect(rect) for unit in settings.UNITS):
                blocked_by_unit = True
                LOGGER.debug("[%s] collides in dir:[%s] with unit", self, dir)

            if not (blocked_by_block or blocked_by_unit):
                self.rect = rect
                break

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)
