from game import settings
from datetime import datetime
import logging
import pygame

LOGGER = logging.getLogger(__name__)

STATES = {
    'attack': 2,
    'move': 1,
    'idle': 0,
}

TEAMS = {
    1: (384, 384),
    2: (32, 32),
}


class Unit(pygame.sprite.Sprite):

    DELAY = 0.1 # seconds
    DEATH = 2

    def __init__(self, team):
        super(Unit, self).__init__()
        self.team = team
        self.target = (0, 0)
        self.target_unit = None
        self.main_target = TEAMS[team]
        self.state = STATES['idle']

        self.dead = False
        self.timer = datetime.now()
        self.time_death = None
        settings.UNITS.add(self)

    def update(self):
        pass

    def render(self, screen):
        pass

    def remove(self):
        if self.time_death and (datetime.now() - self.time_death).total_seconds() > self.DEATH:
            settings.UNITS.remove(self)

    def attack(self, target, damage):
        self.state = STATES['attack']
        target.hp = target.hp - damage

    def move(self, possible_directions):
        """
        :param possible_directions: List of possible directions, if one of them is blocked.
        dirs should be defined like keys in settings.DIRECTIONS. For example ['left', 'top', 'bottom'].
        """
        for dir in possible_directions:
            x, y = settings.DIRECTIONS[dir]
            rect = self.rect.move(x, y)

            blocked_by_block = False
            if any(block.rect.colliderect(rect) for block in settings.OBSTACLES):
                blocked_by_block = True
                LOGGER.debug("[%s] collides in dir:[%s] with block", self, dir)

            blocked_by_unit = False
            if any(unit.rect.colliderect(rect) for unit in settings.UNITS):
                blocked_by_unit = True
                LOGGER.debug("[%s] collides in dir:[%s] with unit", self, dir)

            if not (blocked_by_block or blocked_by_unit):
                self.rect = rect
                break

    def move2(self, direction):
        """
        this method is connected with shortest_path() method from logic module.
        :param direction: tuple
        """
        self.rect.move_ip(direction)
