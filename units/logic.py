import math
from game import settings
from datetime import datetime
from random import shuffle
from buildings.building import Building
from resources.resource import Resource
from units.unit import Unit
import pygame



WALK_SCENARIOS = {
    'left': ['top', 'bottom', 'top_left', 'bottom_left'],
    'top': ['left', 'right', 'top_left', 'top_right'],
    'right': ['top', 'bottom', 'top_right', 'bottom_right'],
    'bottom': ['left', 'right', 'bottom_left', 'bottom_right'],
    'top_right': ['top', 'right'],
    'top_left': ['top', 'left'],
    'bottom_right': ['bottom', 'right'],
    'bottom_left': ['bottom', 'left']
}

def distance(unit_a, unit_b):
    tx, ty = unit_a.rect.center
    cx, cy = unit_b.rect.center
    return math.sqrt((tx - cx)**2 + (ty-cy)**2)

def distance2(target_1, target_2):
    tx, ty = target_1
    cx, cy = target_2
    return math.sqrt((tx - cx) ** 2 + (ty - cy) ** 2)

def get_direction(unit, target):
    cx, cy = unit.rect.x, unit.rect.y
    tx, ty = target

    diff_x = tx - cx
    diff_y = ty - cy
    try:
        dir_x = (diff_x / abs(diff_x)) * settings.CELL_WIDTH
    except ZeroDivisionError:
        dir_x = 0
    try:
        dir_y = (diff_y / abs(diff_y)) * settings.CELL_HEIGHT
    except ZeroDivisionError:
        dir_y = 0

    direction = (0, 0)
    for k, v in settings.DIRECTIONS.iteritems():
        if v == (dir_x, dir_y):
            direction = k
            break

    return direction

def get_walk_scenario(dir):
    walk_dirs = WALK_SCENARIOS[dir]
    shuffle(walk_dirs)
    walk_dirs.insert(0, dir)
    return walk_dirs


def coords_sum(coord_1, coord_2):
    return (coord_1[0] + coord_2[0], coord_1[1] + coord_2[1])


def _unpack_parameter(unit):
    if isinstance(unit, pygame.sprite.Sprite):
        return unit.rect.topleft
    elif isinstance(unit, tuple):
        return unit


def shortest_path(sprite_1, sprite_2, steps=2):
    """
    unit_1 must be a Sprite object.
    """
    x1, y1 = _unpack_parameter(sprite_1)
    x2, y2 = _unpack_parameter(sprite_2)

    DIRS = settings.DIRECTIONS.values()
    #start = datetime.now()
    paths = []
    for value in DIRS:
        paths.append([(x1, y1), coords_sum((x1, y1), value)])

    iteration = steps
    found_path = []
    while not found_path and iteration > 0:
        for path in list(paths):
            for value in DIRS:
                nx, ny = coords_sum(path[-1], value)
                rect = sprite_1.rect.move(nx, ny)
                if (
                    nx >= 0 and ny >= 0 and
                    not pygame.sprite.spritecollideany(rect, settings.OBSTACLES) and
                    not pygame.sprite.spritecollideany(rect, settings.UNITS) and
                    not any((nx, ny) in path for path in paths)
                ):
                    npath = list(path)
                    npath.append((nx, ny))
                    paths.append(npath)
            paths.remove(path)

        for path in paths:
            if path[-1] == (x2, y2):
                found_path = path
                break
        iteration -= 1
    else:
        setup = [(path, math.sqrt((path[-1][0] - x2)**2 + (path[-1][1] - y2)**2)) for path in paths]
        found_path, _ = min(setup, key=lambda t: t[1])

    try:
        x, y = found_path[1]
        first_step = (x - x1, y - y1)
    except IndexError:
        first_step = (0, 0)

    return found_path, first_step


if __name__ == '__main__':
    path, step = shortest_path((128, 0), (256,256), 3)









