import pygame
from unit import Unit, STATES
from game import settings
from game import colors
import logic
from datetime import datetime


class Footman(Unit):

    DAMAGE = 10
    DELAY = 0.1  # seconds
    HP = 60
    ATTACK_RANGE = 16
    VISION = 32

    def __init__(self, x, y, team):
        super(Footman, self).__init__(team)

        self.name = "Footman"
        self.hp = self.HP
        self.color = colors.WHITE
        self.rect = pygame.Rect(x, y, settings.UNIT_WIDTH, settings.UNIT_HEIGHT)
        self.scan_target()

    def __str__(self):
        return "({0}):{1}".format(self.team, self.name)

    def resurrection(self):
        self.hp = self.HP
        self.dead = False
        self.time_death = None

    def death(self):
        if not self.dead and self.hp <= 0:
            self.dead = True
            self.time_death = datetime.now()
        self.remove()

    def scan_target(self):
        """
        Scan method should be different for single unit. If unit is a healer, it should search friends.
        """
        available_enemies = []
        for enemy in filter(lambda unit: not unit.team == self.team, settings.UNITS):
            distance = logic.distance(enemy, self)
            if distance <= self.VISION:
                available_enemies.append((enemy, distance))

        if available_enemies:
            available_enemies.sort(key=lambda en: en[1])
            enemy, _ = available_enemies[0]
            self.target_unit = enemy
            self.target = (enemy.rect.x, enemy.rect.y)
        else:
            self.target_unit = None
            self.target = self.main_target

        self.state = STATES['move']

    def update(self):
        self.death()
        if self.dead:
            return False

        self.scan_target()
        diff = (datetime.now() - self.timer).total_seconds() > self.DELAY
        if diff:
            if self.target_unit and logic.distance(self.target_unit, self) <= self.ATTACK_RANGE:
                self.attack(self.target_unit, self.DAMAGE)
            elif self.state == STATES['move']:
                direction = logic.get_direction(self, self.target)
                dirs = logic.get_walk_scenario(direction)
                self.move(dirs)
            self.timer = datetime.now()

    def render(self, screen):
        pygame.draw.ellipse(screen, self.color, self.rect)

