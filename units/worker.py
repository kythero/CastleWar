from units.unit import Unit
import pygame
from game import settings, colors
from tasks import Tasks
from game.settings import States
from datetime import datetime
import logic
import logging

LOGGER = logging.getLogger(__name__)


class Worker(Unit):

    DAMAGE = 1
    DELAY = 0.1  # seconds
    HP = 40
    ATTACK_RANGE = 16
    VISION = 32
    SPEED = 0.002
    RANGE = 20

    def __init__(self, x, y, team):
        super(Worker, self).__init__(team)
        self.name = "Worker"
        self.hp = self.HP
        self.color = colors.RED
        self.rect = pygame.Rect(x, y, settings.UNIT_WIDTH, settings.UNIT_HEIGHT)
        self.state = States.idle
        self.job = None
        self.hold = None
        self.walk_timer = datetime.now()
        self.target = None

    def __str__(self):
        return self.name

    def update(self):
        if not self.job:
            self.job = Tasks.get(self.team)

            self.state = States.move
        elif self.job:
            object_1, object_2 = self.job
            if self.target is None:
                self.target = object_1

            diff = (datetime.now() - self.walk_timer).total_seconds() > self.SPEED
            if logic.distance2(self.rect.topleft, self.target.rect.topleft) < self.RANGE:
                if not self.hold:
                    self.hold = self.target.withdraw_resource()
                    self.target = object_2
                else:
                    self.target.deposit_resource(self.hold)
                    self.target = None
                    self.job = None
                    self.hold = None
                    self.state = States.idle
            elif diff:
                _, direction = logic.shortest_path(self, self.target)
                self.move2(direction)
                self.walk_timer = datetime.now()
        else:
            self.state = States.idle


    def move(self, possible_directions):
        for dir in possible_directions:
            x, y = settings.DIRECTIONS[dir]
            rect = self.rect.move(x, y)

            blocked_by_block = False
            if any(block.rect.colliderect(rect) for block in settings.OBSTACLES):
                blocked_by_block = True
                LOGGER.debug("[%s] collides in dir:[%s] with block", self, dir)

            blocked_by_unit = False
            if any(unit.rect.colliderect(rect) for unit in settings.BUILDINGS):
                blocked_by_unit = True
                LOGGER.debug("[%s] collides in dir:[%s] with unit", self, dir)

            if not (blocked_by_block or blocked_by_unit):
                self.rect = rect
                break

    def render(self, screen):
        pygame.draw.ellipse(screen, self.color, self.rect.move(-4,-4))
