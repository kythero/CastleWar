from units.unit import Unit
import pygame
from game import settings, colors
from game.settings import States
from datetime import datetime
from resources.tree import Tree
import logic
import sys
import random
from itertools import ifilter


class Lumberman(Unit):

    DAMAGE = 1
    DELAY = 0.1  # seconds
    HP = 40
    ATTACK_RANGE = 12
    RANGE = 20
    VISION = 32
    SPAWN_TREE = 3
    TIME_FOR_TREE = 3
    SPEED = 0.002

    def __init__(self, x, y, team, house):
        super(Lumberman, self).__init__(team)
        self.house = house
        self.name = "Lumberman"
        self.hp = self.HP
        self.color = colors.RED
        self.rect = pygame.Rect(x, y, settings.UNIT_WIDTH, settings.UNIT_HEIGHT)
        self.state = States.idle
        self.target = None
        self.wood = 0
        self.go_to_tree = False
        self.walk_timer = datetime.now()

    def __str__(self):
        return self.name

    def get_tree_location(self):
        for tree in ifilter(lambda o: isinstance(o, Tree), settings.RESOURCES):
            if any(tree.rect.collidepoint(field) for field in self.house.tree_locations):
                return tree
        return None

    def update(self):
        if self.state == States.idle:
            diff = (datetime.now() - self.timer).total_seconds() > self.SPAWN_TREE
            if diff:
                for field in self.house.fields:
                    if not any(unit.rect.collidepoint(field) for unit in settings.UNITS):

                        self.target = self.get_tree_location()
                        if self.target:
                            self.rect.topleft = field
                            self.state = States.move
                            self.go_to_tree = True
                        break

        elif self.state == States.move:
            if logic.distance2(self.target.rect.topleft, self.rect.topleft) < self.RANGE:
                if self.go_to_tree:
                    self.wood = self.target.collect_wood()
                    self.go_to_tree = False
                    self.target = self.house
                else:
                    self.state = States.idle
                    self.house.stored_wood += self.wood
                    self.wood = 0
                    self.rect.x = -8
                    self.rect.y = -8
                    self.timer = datetime.now()
            else:
                diff = (datetime.now() - self.walk_timer).total_seconds() > self.SPEED
                if diff and self.target:
                    _, direction = logic.shortest_path(self, self.target)
                    self.move2(direction)
                    self.walk_timer = datetime.now()

    def render(self, screen):
        pygame.draw.ellipse(screen, self.color, self.rect.move(-4, -4))