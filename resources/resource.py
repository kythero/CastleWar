import pygame
from game.settings import RESOURCES


class Resource(pygame.sprite.Sprite):
    def __init__(self):
        super(Resource, self).__init__()
        RESOURCES.add(self)
