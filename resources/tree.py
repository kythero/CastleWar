from game import settings, colors
from datetime import datetime
import pygame
import random
from resources.resource import Resource


class Tree(Resource):

    GROW_TIME = 60
    WOOD_MIN = 5
    WOOD_MAX = 15
    WOOD = range(WOOD_MIN, WOOD_MAX)

    def __init__(self, x, y):
        super(Tree, self).__init__()
        self.hp = 10
        self.color = colors.GREEN
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH, settings.CELL_HEIGHT)
        self.timer = datetime.now()
        self.ready = False

    def collect_wood(self):
        wood = random.choice(self.WOOD)
        settings.RESOURCES.remove(self)

        return wood

    def update(self):
        diff = (datetime.now() - self.timer).total_seconds() > self.GROW_TIME
        if diff:
            self.ready = True

    def render(self, screen):
        pygame.draw.ellipse(screen, self.color, self.rect.move(-4, -4))
