from queue import LifoQueue
from itertools import ifilter
from datetime import datetime

class MyQueue(object):

    def __init__(self):
        self.queue = []

    def get(self):
        try:
            return self.queue.pop()
        except IndexError:
            return None


    def put(self, job):
        self.queue.insert(0, job)

    def check(self, job):
        element = next(ifilter(lambda j: j == job, self.queue), None)
        return element

    def empty(self):
        return False if len(self.queue) > 0 else True


class Tasks(object):

    USER = MyQueue()
    ENEMY = MyQueue()

    @classmethod
    def player(cls, team):
        return {
            1: cls.USER,
            2: cls.ENEMY
        }[team]

    @classmethod
    def put(cls, team, job=()):
        if not cls.player(team).check(job):
            cls.player(team).put(job)

    @classmethod
    def get(cls, team):
        if not cls.player(team).empty():
            biore = cls.player(team).get()

            return biore