import pygame
from buildings.barrack import LumberMill


class Sprite(object):

    def __init__(self, x, y, width, height, color):
        self.surface = pygame.Surface((width, height))
        self.rect = self.surface.fill(color)
        self.rect.topleft = (x, y)
        self.building = LumberMill

    def render(self, screen):
        screen.blit(self.surface, self.rect.topleft)