from position import Position

class Field(object):

    def __init__(self, x=None, y=None, landscape=None):
        self.unit = None
        self.x = x
        self.y = y
        self.landscape = landscape
        self.available = True

    def __eq__(self, other):
        return True if self.x == other.x and self.y == other.y else False

    def convert(self, other):
        self.unit = other.unit

    def reset(self):
        self.unit = None

