from constants import Constants
from field import Field
from position import Position
from itertools import ifilter


class Grid(object):
    GRID = set()

    def __new__(cls, *args, **kwargs):
        if getattr(cls, "_singleton", None) is None:
            cls._singleton = super(Grid, cls).__new__(cls, *args, **kwargs)
            cls.GRID = set()
            for x in range(0, Constants.MAP_WIDTH, Constants.CELL):
                for y in range(0, Constants.MAP_HEIGHT, Constants.CELL):
                    cls.GRID.add(Field(x, y))
        return cls._singleton

    def add(self, entity, x, y):
        cell = self.get_cell_by_pos(x, y)
        cell.unit = entity

    def movable(self, position):
        cell = self.get_cell_by_field(position)
        return False if cell.unit else cell.position

    def move(self, mx, my, entity):
        curr_position = self.get_cell_by_unit(entity)
        new_position = self.get_cell_by_pos(curr_position.x + mx, curr_position.y + my)
        new_position.convert(curr_position)
        entity.cell = new_position
        curr_position.reset()




    def get_cell_by_field(self, ifield):
        return ifilter(lambda field: field == ifield, self.GRID).next()

    def get_cell_by_pos(self, *args):
        return ifilter(lambda field: field.x == args[0] and field.y == args[1], self.GRID).next()

    def get_cell_by_unit(self, unit):
        return ifilter(lambda field: field.unit == unit, self.GRID).next()

    def register(self, x, y, unit):
        field = self.get_cell_by_pos(x, y)
        field.unit = unit
        return field

    def cell_available(self, coord):
        cell = self.get_cell_by_pos(*coord)
        if cell.available and not cell.unit:
            return True
        return False


if __name__ == '__main__':
    print Grid()
    print Grid().movable(Position(0,0))






