import math


class Position(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return True if self.x == other.x and self.y == other.y else False

    def __add__(self, other):
        return Position(self.x + other.x, self.y + other.y)

    def move(self, pair):
        return Position(self.x + pair[0], self.y + pair[1])

