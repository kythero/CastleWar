"""
Units are placed with (x,y) no matter how big cell is.
All logic about target/distance is calculated from centers.
Images in future are placed on (x,y) with offset of its width and height : (x-width/2, y-height/2)
"""
import logging
import pygame
from collections import namedtuple

log_level = logging.DEBUG

logging.basicConfig(level=log_level, format="[%(asctime)s][%(filename)s]: %(message)s")

WIDTH = 800
HEIGHT = 600
TITLE = 'title'

UNIT_WIDTH = 8
UNIT_HEIGHT = 8

CELL_WIDTH = 8
CELL_HEIGHT = 8

UNITS = pygame.sprite.Group()
BUILDINGS = pygame.sprite.Group()
OBSTACLES = pygame.sprite.Group()
RESOURCES = pygame.sprite.Group()

DIRECTIONS = {
    'top': (0, -1),
    'top_right': (1, -1),
    'right': (1, 0),
    'bottom_right': (1, 1),
    'bottom': (0, 1),
    'bottom_left': (-1, 1),
    'left': (-1, 0),
    'top_left': (-1, -1)
}

_directions = namedtuple(
    'Directions',
    ['top', 'top_right', 'right', 'bottom_right', 'bottom', 'bottom_left', 'left', 'top_left', 'none']
)
Directions = _directions((0, -8), (8, -8), (8, 0), (8, 8), (0, 8), (-8, 8), (-8, 0), (-8, -8), (0, 0))

_states = namedtuple('States', ['idle', 'move', 'attack'])
States = _states(0, 1, 2)



