from abc import ABCMeta, abstractmethod


class AnimationHelper(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def blink(self, entity, speed, min, max):
        """
        Class modify surface inside entity class.

        :param entity: the Entity class.
        :param speed: default 1.
        :param min: minimum value for fade. Default 0.
        :param max: maximum value for fade. Default 255.
        :raise: * AttributeError if entity class does not have attribute surface.
        :return: None
        """
