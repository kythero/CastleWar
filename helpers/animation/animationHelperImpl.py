from animationHelper import AnimationHelper


class AnimationHelperImpl(AnimationHelper):

    def blink(self, surface, speed=1, min=0, max=255):

        if getattr(surface, "_alpha", None) is None:
            setattr(surface, "_alpha", 255)

        if getattr(surface, "_fade_in", None) is None:
            setattr(surface, "_fade_in", True)

        if surface._fade_in:
            surface._alpha -= speed
            if surface._alpha <= min:
                surface._fade_in = False
        else:
            surface._alpha += speed
            if surface._alpha >= max:
                surface._fade_in = True

        surface.set_alpha(surface._alpha)


