from abc import ABCMeta, abstractmethod


class BuildingHelper(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def spawn_cells(self, x, y, area, cell_size):
        """
        * collect fields where you can spawn unit, near building.

        :return: tuples(coords: (x,y)) for possible spawn.
        """