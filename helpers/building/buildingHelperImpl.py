from buildingHelper import BuildingHelper
from constants import Constants


class BuildingHelperImpl(BuildingHelper):

    def spawn_cells(self, x, y, area, cell_size=Constants.CELL):
        fields = set()

        for x in range(x - area, x + area + cell_size, cell_size):
            for y in range(y - area, y + area + cell_size, cell_size):
                fields.add((x, y))

        inner_fields = set()
        for x in range(x - area + cell_size, x + area, cell_size):
            for y in range(y - area + cell_size, y + area, cell_size):
                inner_fields.add((x, y))

        return tuple(fields - inner_fields)
