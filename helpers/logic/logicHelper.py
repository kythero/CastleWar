from abc import ABCMeta, abstractmethod


class LogicHelper(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def direction(self, x, y):
        """
        :param x:
        :param y:
        :return:
        """