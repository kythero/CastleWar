from abc import ABCMeta, abstractmethod


class UnitHelper(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def spawn_cells(self, x, y, area):
        """
        :param x:
        :param y:
        :param area:

        :return: possible cells, around point.
        """