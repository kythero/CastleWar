from unitHelper import UnitHelper
from constants import Constants
from grid.grid import Grid

class UnitHelperImpl(UnitHelper):

    def spawn_cells(self, x, y, cell_size=Constants.CELL):
        fields = set()

        for x in range(x, x + cell_size*2, cell_size):
            for y in range(y, y + cell_size*2, cell_size):
                fields.add((x, y))

        fields.remove((x,y))

        for field in tuple(fields):
            if not Grid().cell_available(field):
                fields.remove(field)

        return tuple(fields)