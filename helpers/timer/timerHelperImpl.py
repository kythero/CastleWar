from timerHelper import TimerHelper
from datetime import datetime

class TimerHelperImpl(TimerHelper):

    TIME_FORMAT = ""

    def now(self):
        return datetime.now()

    def elapsed(self, from_time, how_long):
        return (datetime.now() - from_time).total_seconds() >= how_long

    def format(self):
        return self.TIME_FORMAT