from abc import ABCMeta, abstractmethod


class TimerHelper(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def now(self):
        """
        Current datetime.
        :return: datetime object.
        """

    @abstractmethod
    def elapsed(self, from_time, how_long):
        """
        :param from_time: datetime
        :param how_long: seconds
        :return: datetime with different
        """

    @abstractmethod
    def format(self):
        """
        :return: string. usable format for datetime.
        """