from building.buildingHelperImpl import BuildingHelperImpl
from unit.unitHelperImpl import UnitHelperImpl
from timer.timerHelperImpl import TimerHelperImpl
from animation.animationHelperImpl import AnimationHelperImpl
from logic.logicHelperImpl import LogicHelperImpl

class Helper(object):

    buildingHelper = BuildingHelperImpl()
    unitHelper = UnitHelperImpl()
    timerHelper = TimerHelperImpl()
    animationHelper = AnimationHelperImpl()
    logicHelper = LogicHelperImpl()

    def __new__(cls, *args, **kwargs):
        if getattr(cls, "_singleton", None) is None:
            cls._singleton = super(Helper, cls).__new__(*args, **kwargs)

        return cls._singleton
