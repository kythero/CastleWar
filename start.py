import pygame

from game import settings
from game import colors
import sys
from grid.grid import Grid


from entity.buildings.building_factory import BuildingFactory
from entity.units.unit_factory import UnitFactory




class Frame(object):

    def __new__(cls, *args, **kwargs):
        if getattr(cls, "_singleton", None) is None:
            cls._singleton = super(Frame, cls).__new__(cls, *args, **kwargs)

            pygame.init()
            cls.RUNNING = 1
            cls.SCREEN = pygame.display.set_mode((settings.WIDTH, settings.HEIGHT))
            pygame.display.set_caption(settings.TITLE)

        return cls._singleton

    def init(self):
        BuildingFactory.town_hall(64, 64, 1)

    def execute(self):
        self.init()
        while self.RUNNING:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit(0)

            self.update()
            self.render()
        pygame.quit()


    def update(self):
        for cell in Grid().GRID:
            if cell.unit:
                cell.unit.update()

    def render(self):
        self.SCREEN.fill(colors.BLACK)

        for cell in Grid().GRID:
            if cell.unit:
                cell.unit.render(self.SCREEN)


        pygame.display.update()


if __name__ == '__main__':
    Frame().execute()




