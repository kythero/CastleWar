import pygame
from game import settings, colors
from building import Building
from datetime import datetime
from units.forester import Forester
import random
from game import settings

class ForesterHouse(Building):

    def __init__(self, x, y, team):
        super(ForesterHouse, self).__init__(team)
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH*2, settings.CELL_HEIGHT*2)
        self.color = colors.BLUE
        self.name = "Forester's House"
        self.fields = self.get_fields(self.rect)
        self.places_for_tree = self.tree_fields(x, y)
        Forester(-8, -8, self.team, self)

    def tree_fields(self, x, y, area=5):
        fields = []
        for ix in range(x - 8 * area, x + 8 * (2 + area), 8):
            for iy in range(y - 8 * area, y + 8 * (2 + area), 8):
                if not (
                    any(obstacle.rect.collidepoint(ix, iy) for obstacle in settings.OBSTACLES) or
                    any(building.rect.collidepoint(ix, iy) for building in settings.BUILDINGS)
                ):
                    fields.append((ix,iy))
        return fields

    def update(self):
        pass

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect.move(-8, -8))
