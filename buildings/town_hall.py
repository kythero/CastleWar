import pygame
from buildings.building import Building
from datetime import datetime
from game import colors, settings
from units.worker import Worker
from grid.grid import Grid

class TownHall(object):

    SPAWN = 10

    def __init__(self, x, y, team):
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH * 5, settings.CELL_HEIGHT * 5)

        self.color = colors.YELLOW
<<<<<<< HEAD
        #self.fields = self.get_fields(self.rect)
        self.team = team
        self.timer = datetime.now()
        Grid().add(self, x, y)
=======
        self.fields = self.get_fields(self.rect)
<<<<<<< HEAD
>>>>>>> bb8dfed444deb6e65ebc5da2785e2bb0fc220627
=======
>>>>>>> bb8dfed444deb6e65ebc5da2785e2bb0fc220627

    def update(self):
        self.spawn_unit(Worker)

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)

    def spawn_unit(self, unit_class):
        diff = (datetime.now() - self.timer).total_seconds() > self.SPAWN
        if diff:
            for field in self.fields:
                x, y = field
                if not any(unit.rect.collidepoint(x, y) for unit in settings.UNITS):
                    unit_class(x, y, self.team)
                    break
            self.timer = datetime.now()