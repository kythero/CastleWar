from datetime import datetime
import pygame
from game.settings import BUILDINGS


class Building(pygame.sprite.Sprite):

    def __init__(self, team):
        super(Building, self).__init__()
        self.team = team
        self.timer = datetime.now()
        BUILDINGS.add(self)

    def withdraw_resource(self):
        pass

    def deposit_resource(self, item):
        pass

    def update(self):
        pass

    def render(self, screen):
        pass

    def get_fields(self, rect):
        tl_x, tl_y = rect.topleft
        tr_x, tr_y = rect.topright
        bl_x, bl_y = rect.bottomleft
        br_x, br_y = rect.bottomright

        top = sorted(range(tl_x - 8, tr_x, 8), reverse=True)
        right = sorted(range(tr_y - 8, br_y, 8), reverse=True)
        bottom = range(bl_x - 8, br_x, 8)
        left = range(tl_y - 8, bl_y, 8)

        # starts from bottom -> right -> top -> left
        fields = zip(bottom, [bl_y] * len(bottom))
        fields.extend(
            zip([tr_x] * len(right), right)
        )
        fields.extend(
            zip(top, [tl_y] * len(top))
        )
        fields.extend(
            zip([tl_x] * len(left), left)
        )

        return fields