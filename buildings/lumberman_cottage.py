import pygame
from game import settings, colors
from building import Building
from datetime import datetime
from units.lumberman import Lumberman

class LumbermanCottage(Building):

    def __init__(self, x, y, team):
        super(LumbermanCottage, self).__init__(team)
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH*2, settings.CELL_HEIGHT*2)
        self.color = colors.GREEN
        self.name = "LumberManCottage"
        self.fields = self.get_fields(self.rect)
        self.tree_locations = self.tree_location(x, y)
        Lumberman(-8, -8, self.team, self)
        self.stored_wood = 0

    def tree_location(self, x, y, area=15):
        fields = [
            (ix, iy) for ix in range(x - 8 * area, x + 8 * area, 8)
                     for iy in range(y - 8 * area, y + 8 * area, 8)
        ]
        return fields

    def withdraw_resource(self):
        if self.stored_wood > 0:
            self.stored_wood -= 1
            return 1

    def deposit_resource(self, item):
        pass

    def update(self):
        pass

    def render(self, screen):

        pygame.draw.rect(screen, self.color, self.rect.move(-8, -8))
        pygame.draw.line(screen, (0,0,0), self.rect.topleft, self.rect.topleft)