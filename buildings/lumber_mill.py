import pygame
from game import settings, colors
from building import Building
from datetime import datetime
from bank import Resources
from tasks import Tasks

class LumberMill(Building):

    WOOD_COLLECT = 10
    SEARCH = 3

    def __init__(self, x, y, team):
        super(LumberMill, self).__init__(team)
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH * 3, settings.CELL_HEIGHT * 3)
        self.name = "LumberMill"
        self.color = colors.YELLOW
        self.stored_wood = 0
        self.boards = 0
        self.timer = datetime.now()
        self.search_wood = datetime.now()

    def withdraw_resource(self):
        pass

    def deposit_resource(self, item):
        self.stored_wood += item
        Resources.add_woods(self.team, item)

    def update(self):
        diff = (datetime.now() - self.search_wood).total_seconds() > self.SEARCH
        if diff:
            lumberman_cottage = [b for b in settings.BUILDINGS if b.name == "LumberManCottage"]
            for cottage in lumberman_cottage:
                if cottage.stored_wood > 0:
                    Tasks.put(self.team, job=(cottage, self))

            self.search_wood = datetime.now()

        diff = (datetime.now() - self.timer).total_seconds() > self.WOOD_COLLECT
        if diff and self.stored_wood > 0:
            self.stored_wood -= 1
            self.boards += 5
            Resources.remove_woods(self.team, 1)
            Resources.add_boards(self.team, 5)
            self.timer = datetime.now()

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)

