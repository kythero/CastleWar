import pygame
from game import settings, colors
from building import Building
from datetime import datetime
from units.footman import Footman

class LumberMill(Building):

    SPAWN = 5  # seconds

    def __init__(self, x, y, team):
        super(LumberMill, self).__init__(team)
        self.rect = pygame.Rect(x, y, settings.CELL_WIDTH*3, settings.CELL_HEIGHT*3)
        self.color = colors.YELLOW
        self.fields = self.get_fields(self.rect)

    def update(self):
        self.spawn_unit(Footman)

    def render(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)

    def spawn_unit(self, unit_class):
        diff = (datetime.now() - self.timer).total_seconds() > self.SPAWN
        if diff:
            for field in self.fields:
                x, y = field
                if not any(unit.rect.collidepoint(x, y) for unit in settings.UNITS):
                    unit_class(x, y, self.team)
                    break
            self.timer = datetime.now()




